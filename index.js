const themeCookieName = 'theme'
const themeLight = 'light'
const body = document.getElementsByTagName('body')[0]

// Ici on donne la liste des servers
var list_server = ['http://localhost:2345','http://localhost:2435']
var nbActives = 0
var nbChatrooms = 0


function setCookie(cname, cvalue, exdays) {
  var d = new Date()
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000))
  var expires = "expires="+d.toUTCString()
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/"
}

function getCookie(cname) {
  var name = cname + "="
  var ca = document.cookie.split(';')
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1)
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length)
    }
  }
  return ""
}

loadTheme()

function loadTheme() {
	var theme = getCookie(themeCookieName)
	body.classList.add(theme === "" ? themeLight : theme)
}



function collapseSidebar() {
	body.classList.toggle('sidebar-expand')
}

window.onclick = function(event) {
	openCloseDropdown(event)
}

function closeAllDropdown() {
	var dropdowns = document.getElementsByClassName('dropdown-expand')
	for (var i = 0; i < dropdowns.length; i++) {
		dropdowns[i].classList.remove('dropdown-expand')
	}
}

function openCloseDropdown(event) {
	if (!event.target.matches('.dropdown-toggle')) {
		// 
		// Close dropdown when click out of dropdown menu
		// 
		closeAllDropdown()
	} else {
		var toggle = event.target.dataset.toggle
		var content = document.getElementById(toggle)
		if (content.classList.contains('dropdown-expand')) {
			closeAllDropdown()
		} else {
			closeAllDropdown()
			content.classList.add('dropdown-expand')
		}
	}
}

async function getUsers(server){
	const url_api = server+'/users'
	const response = await fetch(url_api);
	const data = await response.json();
	var Users = Array.of(data);

	var table = document.getElementById('myUsers')
	var count = 0;
	for (var i = 0; i < Users[0].length; i++){
		if(Users[0][i].currentStatus == 'ACTIVE'){
			count++
		}
		var row = `<tr>
						<td>${server}</td>
						<td>${Users[0][i].account.id}</td>
						<td>${Users[0][i].account.username}</td>
						<td>${Users[0][i].currentStatus}</td>
				</tr>`
		table.innerHTML += row

	}
	nbActives = nbActives + count
}
async function getChatrooms(server){
	
    const url_api = server+'/chatrooms'
	const response = await fetch(url_api);
	const data = await response.json();
	var Chatrooms = Array.of(data);
	var table = document.getElementById('myChatrooms')

	for (var i = 0; i < Chatrooms[0].length; i++){
		var row = `<tr>
						<td>${server}</td>
						<td>${Chatrooms[0][i]}</td>

				</tr>`
		table.innerHTML += row
	}
	
	 nbChatrooms = nbChatrooms +  Chatrooms[0].length
}


async function loadData(servers){
	for (var i = 0; i < servers.length; i++){
	    getUsers(servers[i])
		getChatrooms(servers[i])
	}
	document.getElementById("actives").innerHTML = nbActives
	document.getElementById("chatrooms").innerHTML = nbChatrooms
}
loadData(list_server)

function displayServers(){
	var str= ' ';
	for (var i = 0; i < list_server.length; i++){
		str=str+list_server[i]
	}
	 document.getElementById("servers_chat").innerHTML = str
	 document.getElementById("servers_usr").innerHTML = str
}
displayServers()

